<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome(Request $request){
        $nama_depan = $request['first-name'];
        $nama_belakang = $request['last-name'];
        return view('welcome', compact('nama_depan', 'nama_belakang'));
    }
    
    public function form(){
        return view('form');
    }//

}
