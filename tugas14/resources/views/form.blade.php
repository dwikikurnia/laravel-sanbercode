<!DOCTYPE html>
<html>

<head>
    <title>SanberBook</title>
</head>

<body>

    <h1>Buat Account Baru!</h1> 
    <h2>Sign Up Form</h2>

    {{-- <form action="welcome" method="POST">
        <label for="">Nama</label><br>
        <input type="text" name="nama_lengkap"><br>
        <label for="">Alamat</label><br>
        <textarea name="address" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="kirim">
    </form> --}}
    <form action="welcome">
        <label for="first-name">First name:</label><br><br>
        <input type="text" name="first-name"><br><br>

        <label for="last-name">Last name:</label><br><br>
        <input type="text" name="last-name"><br><br>

        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label for="nation">Nationality:</label><br><br>
        <select name="nation">
            <option value="Indonesian">Indonesian</option>
            <option value="Chinese">Chinese</option>
            <option value="Australian">Australian</option>
            <option value="Other">Other</option>
        </select><br><br>

        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>

        <label for="Bio">Bio:</label><br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
          

</body>

</html>