<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    
    public function create(){
        return view('posts.create');
    }
    
    public function store(Request $request){
        // dd($request->all());
        $query = DB::table('cast')->insert([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]);
        return redirect('/cast/create');
        // return view('posts.store');
    }

    public function index(){
        return view('posts.index');
    }

    public function show(){
        return view('show');
    }

    public function edit(){
        return view('edit');
    }

    public function update(){
        return view('update');
    }

    public function destroy(){
        return view('destroy');
    }  
    //
}
